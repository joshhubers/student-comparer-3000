export type Student = {
    id: number,
    name: string,
    hp: number,
    dps: number
    avatar: string,
}

export type PlaceholderUser = {
    id: number,
    name: string,
}

export type CompetitionResult = Student | null | "tie";

export type StudentEditArgs = { name: string, hp: number, dps: number };