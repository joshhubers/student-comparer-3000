import { PlaceholderUser, Student } from '../types';

const placeHolderUserUrl = "https://jsonplaceholder.typicode.com/users";
let loadedUsers: PlaceholderUser[] | null = null;

async function fetchUsers(): Promise<PlaceholderUser[]> {
    if (loadedUsers != null)
        return loadedUsers;

    const response = await fetch(placeHolderUserUrl);

    if (response.ok) {
        const users = await response.json() as PlaceholderUser[];
        loadedUsers = users;
        return loadedUsers;
    } else {
        alert("Failed retrieving user data, mock data will be used");

        return [
            {
                id: 1,
                name: "George Washington"
            },
            {
                id: 2,
                name: "Abe Lincoln"
            },
        ]
    }
}

export async function getStudents(): Promise<Student[]> {
    const users = await fetchUsers();
    return users.map(u => {
        return {
            id: u.id,
            name: u.name,
            hp: simpleRand() * -1,
            dps: simpleRand(),
            avatar: `https://avatars.dicebear.com/api/bottts/${u.name}.svg?b=%23FFF`,
        }
    });
}

export async function getRandomPair(): Promise<Student[]> {
    const students = await getStudents();
    const first = randomStudent(students);
    const second = randomStudent(students, [first]);

    return [first, second];
}

export function randomStudent(students: Student[], excluded?: Student[]) {
    const excludedIds = excluded?.map(s => s.id) ?? [];
    const withExlusion = students.filter(s => !excludedIds?.some(id => id === s.id));

    return withExlusion[Math.floor(Math.random() * withExlusion.length)];
}

const simpleRand = (): number => Math.floor(Math.random() * 100) + 1;