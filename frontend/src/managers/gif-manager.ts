type GiphyGif = {
    id: string
    embed_url: string
}

type GiphyResponse = {
    data: GiphyGif[];
}

let loadedGiphyResponse: GiphyGif[] | null = null;

const lookupKeyword = 'celebrate';
const lookupUrl = `https://api.giphy.com/v1/gifs/search?api_key=qAD6ZR8484fEpoMyeFAT0GdT7hDPrKH8&q=${lookupKeyword}&limit=25&offset=0&rating=g&lang=en`;

export const getGifs = async () => {
    if (loadedGiphyResponse != null)
        return loadedGiphyResponse;

    const response = await fetch(lookupUrl);

    if (response.ok) {
        const giphyJson = await response.json() as GiphyResponse;
        loadedGiphyResponse = giphyJson.data;
        return loadedGiphyResponse;
    }
}

export async function getRandomGif(): Promise<GiphyGif> {
    const gifs = await getGifs();
    if (gifs) {
        return gifs[Math.floor(Math.random() * gifs.length)];
    } else {
        return { id: "placeholder", embed_url: "http://gph.is/1gWXLWn" }
    }
}