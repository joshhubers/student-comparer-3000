import { AppBar, Toolbar } from "@material-ui/core";
import * as React from "react";
import "./AppBanner.css";

export const AppBanner = () => {
  return (
    <AppBar>
      <Toolbar>
        <h1>Student Laugh Competition</h1>
      </Toolbar>
    </AppBar>
  );
};
