import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  TextField,
  Typography,
} from "@material-ui/core";
import * as React from "react";
import { useState } from "react";
import { Student, StudentEditArgs } from "../types";
import "./EditStudentDialog.css";

export const EditStudentDialog = (props: {
  student: Student;
  onClose: (studentArgs?: StudentEditArgs) => void;
}) => {
  const [state, setState] = useState<{
    studentArgs: StudentEditArgs;
    validationError: boolean;
  }>({ studentArgs: { ...props.student }, validationError: false });

  const trySave = () => {
    const valid =
      isValidNumber(state.studentArgs.dps, 0, 100) &&
      isValidNumber(state.studentArgs.hp, -101, -1);
    if (valid) props.onClose(state.studentArgs);
    else {
      setState({ ...state, validationError: true });
    }
  };

  const isValidNumber = (num: number, lower: number, upper: number) => {
    return num > lower && num <= upper;
  };

  const getValidationError = () => {
    if (state.validationError) {
      return (
        <div>
          <Typography className="text-center" color="error">
            Health must be between -1 and -100
          </Typography>
          <Typography className="text-center" color="error">
            DPS must be between 1 and 100
          </Typography>
        </div>
      );
    }
  };

  return (
    <Dialog open={true} onClose={(_) => props.onClose()}>
      <DialogTitle>Editing {props.student.name}</DialogTitle>
      <DialogContent>
        <List>
          <ListItem>
            <TextField
              className="full-width"
              onChange={(e) =>
                setState({
                  ...state,
                  studentArgs: { ...state.studentArgs, name: e.target.value },
                })
              }
              helperText="Name"
              value={state.studentArgs.name}
            />
          </ListItem>
          <ListItem>
            <TextField
              onChange={(e) =>
                setState({
                  ...state,
                  studentArgs: {
                    ...state.studentArgs,
                    hp: Number(e.target.value),
                  },
                })
              }
              helperText="HP"
              type="number"
              value={state.studentArgs.hp}
            />
          </ListItem>
          <ListItem>
            <TextField
              onChange={(e) =>
                setState({
                  ...state,
                  studentArgs: {
                    ...state.studentArgs,
                    dps: Number(e.target.value),
                  },
                })
              }
              helperText="DPS"
              type="number"
              value={state.studentArgs.dps}
            />
          </ListItem>
        </List>
      </DialogContent>
      {getValidationError()}
      <DialogActions>
        <Button onClick={(_) => props.onClose()}>Cancel</Button>
        <Button onClick={(_) => trySave()}>Save</Button>
      </DialogActions>
    </Dialog>
  );
};
