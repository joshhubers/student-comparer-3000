import { Button } from "@material-ui/core";
import { Autorenew } from "@material-ui/icons";
import * as React from "react";
import "./ContestController.css";

export const ContestController = (props: {
  onCompete: () => void;
  onShuffle: () => void;
}) => {
  return (
    <div className="control-container">
      <Button
        onClick={(_) => props.onCompete()}
        variant="contained"
        color="primary"
      >
        Compete!
      </Button>
      <Button
        className="margin-top"
        onClick={(_) => props.onShuffle()}
        variant="contained"
        color="primary"
      >
        <Autorenew />
      </Button>
    </div>
  );
};
