import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@material-ui/core";
import * as React from "react";
import { useEffect, useState } from "react";
import { getRandomGif } from "../managers/gif-manager";
import { CompetitionResult } from "../types";
import "./VictoryDialog.css";

export const VictoryDialog = (props: {
  result: CompetitionResult;
  onClose: () => void;
}) => {
  const [state, setState] = useState<{ loadedGif: string | undefined }>({
    loadedGif: undefined,
  });

  useEffect(() => {
    if (!state.loadedGif) {
      getRandomGif().then((randomGif) => {
        setState({ loadedGif: randomGif.embed_url });
      });
    }
  });

  const modalContent = () => {
    if (props.result === "tie") {
      return (
        <div>
          <DialogTitle>So close</DialogTitle>
          <DialogContent>
            <h1>It's a tie!</h1>
          </DialogContent>
        </div>
      );
    } else {
      return (
        <div>
          <DialogTitle>Victory!</DialogTitle>
          <DialogContent>
            <h1>{props.result!.name} is the winner!</h1>
            <iframe
              title="victory gif"
              className="gifFrame"
              frameBorder="0"
              src={state?.loadedGif}
            />
          </DialogContent>
        </div>
      );
    }
  };

  return (
    <Dialog open={true} onClose={props.onClose}>
      {modalContent()}
      <DialogActions>
        <Button onClick={props.onClose}>Close</Button>
      </DialogActions>
    </Dialog>
  );
};
