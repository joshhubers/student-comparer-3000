import { Grid } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import {
  getRandomPair,
  getStudents,
  randomStudent,
} from "../managers/student-manager";
import { CompetitionResult, Student, StudentEditArgs } from "../types";
import { ContestController } from "./ContestController";
import { Loader } from "./Loader";
import { StudentCard } from "./StudentCard";
import { VictoryDialog } from "./VictoryDialog";

type State = {
  studentOne: Student | null;
  studentTwo: Student | null;
  competitionResult: CompetitionResult;
  isLoading: Boolean;
  hasLoaded: Boolean;
  showWinnerModal: Boolean;
};

const initState: State = {
  studentOne: null,
  studentTwo: null,
  competitionResult: null,
  isLoading: true,
  hasLoaded: false,
  showWinnerModal: false,
};

export const StudentComparer = () => {
  const [state, setState] = useState<State>(initState);

  // Research showed using async inside of useEffect isn't obvious. So I've decided to use .then() for KISS.
  // Still learning React 🤷‍♂️
  useEffect(() => {
    if (!state.hasLoaded) {
      getRandomPair().then((randomPair) => {
        setState({
          ...state,
          isLoading: false,
          hasLoaded: true,
          studentOne: randomPair[0],
          studentTwo: randomPair[1],
          competitionResult: null,
        });
      });
    }
  });

  const getWinnerModal = () => {
    if (state.showWinnerModal && state.competitionResult) {
      return (
        <VictoryDialog
          result={state.competitionResult}
          onClose={() => setState({ ...state, showWinnerModal: false })}
        />
      );
    }
  };

  const shuffleStudents = () => {
    getRandomPair().then((randomPair) => {
      setState({
        ...state,
        studentOne: randomPair[0],
        studentTwo: randomPair[1],
        competitionResult: null,
      });
    });
  };

  const competeStudents = (
    firstStudent: Student,
    secondStudent: Student
  ): CompetitionResult => {
    const hitsForOne = firstStudent.hp / (secondStudent.dps * -1);
    const hitsForTwo = secondStudent.hp / (firstStudent.dps * -1);

    if (hitsForOne <= 1 && hitsForTwo <= 1) {
      return "tie";
    } else if (hitsForOne > hitsForTwo) {
      return firstStudent;
    } else if (hitsForOne < hitsForTwo) {
      return secondStudent;
    } else {
      return "tie";
    }
  };

  const competeStudentsAndShow = () => {
    if (!state.studentOne || !state.studentTwo) return;

    const result = competeStudents(state.studentOne, state.studentTwo);
    setState({ ...state, showWinnerModal: true, competitionResult: result });
  };

  const editStudent = (id: number, studentArgs: StudentEditArgs) => {
    if (state.studentOne!.id === id) {
      const editedStudent = Object.assign(state.studentOne, studentArgs);
      setState({ ...state, studentOne: editedStudent });
    } else {
      const editedStudent = Object.assign(state.studentTwo, studentArgs);
      setState({ ...state, studentTwo: editedStudent });
    }
  };

  const setRandomStudent = async (
    chosenStudent: "studentOne" | "studentTwo"
  ) => {
    if (chosenStudent === "studentOne") {
      const newStudent = randomStudent(await getStudents(), [
        state.studentOne!,
        state.studentTwo!,
      ]);
      setState({ ...state, studentOne: newStudent });
    } else {
      const newStudent = randomStudent(await getStudents(), [
        state.studentOne!,
        state.studentTwo!,
      ]);
      setState({ ...state, studentTwo: newStudent });
    }
  };

  const getContent = () => {
    if (state.isLoading) {
      return <Loader />;
    } else {
      return (
        <Grid container spacing={2}>
          <Grid item xs={5}>
            <StudentCard
              student={state.studentOne!}
              onEditStudent={(args) => editStudent(state.studentOne!.id, args)}
              onShuffle={() => setRandomStudent("studentOne")}
            />
          </Grid>

          <Grid item xs={2}>
            <ContestController
              onCompete={competeStudentsAndShow}
              onShuffle={shuffleStudents}
            />
          </Grid>

          <Grid item xs={5}>
            <StudentCard
              student={state.studentTwo!}
              onEditStudent={(args) => editStudent(state.studentTwo!.id, args)}
              onShuffle={() => setRandomStudent("studentTwo")}
            />
          </Grid>

          {getWinnerModal()}
        </Grid>
      );
    }
  };

  return getContent();
};
