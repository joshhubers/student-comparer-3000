import { Avatar, Button, ButtonGroup, Paper } from "@material-ui/core";
import { Autorenew, Edit } from "@material-ui/icons";
import * as React from "react";
import { useState } from "react";
import { Student, StudentEditArgs } from "../types";
import { EditStudentDialog } from "./EditStudentDialog";
import "./StudentCard.css";

export function StudentCard(props: {
  student: Student;
  onEditStudent: (args: StudentEditArgs) => void;
  onShuffle: () => void;
}) {
  const [state, setState] = useState<{ showEditDialog: boolean }>({
    showEditDialog: false,
  });

  const onEditClose = (studentArgs?: StudentEditArgs) => {
    setState({ showEditDialog: false });

    if (studentArgs) props.onEditStudent(studentArgs);
  };

  return (
    <Paper className="student-card">
      <Avatar className="centered" src={props.student.avatar} />
      <h1 className="student-title">{props.student.name}</h1>
      <div>
        <h1>HP: {props.student.hp}</h1>
      </div>
      <div>
        <h1>DPS: {props.student.dps}</h1>
      </div>
      <ButtonGroup>
        <Button onClick={(_) => setState({ showEditDialog: true })}>
          <Edit />
        </Button>
        <Button onClick={(_) => props.onShuffle()}>
          <Autorenew />
        </Button>
      </ButtonGroup>

      {state.showEditDialog && (
        <EditStudentDialog student={props.student} onClose={onEditClose} />
      )}
    </Paper>
  );
}
