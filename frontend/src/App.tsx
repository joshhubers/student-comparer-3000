import React from "react";
import "./App.css";
import { StudentComparer } from "./components/StudentComparer";
import { Container, Divider } from "@material-ui/core";
import { AppBanner } from "./components/AppBanner";

function App() {
  return (
    <div>
      <AppBanner />
      <Container>
        <div className="margin-top">
          <div className="instructions">
            <p>
              The goal is to find the winner in a cheering up contest on a video
              call. Each student starts with some negative amount of HP
              (happiness points). The rate at which they can cheer up (add HP)
              other students is their DPS (delight per second). Students laugh
              when they get to a positive HP.
            </p>
          </div>
          <Divider />
          <div className="margin-bottom"></div>
          <StudentComparer />
        </div>
      </Container>
    </div>
  );
}

export default App;
