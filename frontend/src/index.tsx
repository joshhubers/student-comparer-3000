import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

const theme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: {
      main: '#5e81ac',
      contrastText: '#eceff4'
    },
    secondary: {
      main: '#bf616a',
      contrastText: '#eceff4'
    },
    background: {
      default: '#2e3440',
      paper: '#4c566a'
    },
    text: {
      primary: '#eceff4'
    }
  },
})

ReactDOM.render(
  <React.StrictMode>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
